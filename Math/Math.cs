﻿// Roger Ribas
// 7/12/22
//// Descripcio: Copia aquest codi. Soluciona tots els errors i warnings, per tal que s'executi. Depura'l, pas a pas fins que arribis a una conclusió i contesta a les següents preguntes, mitjançant comentaris al final del main:
//Quin valor té result quan la i == 1000?
//El valor result és mai major que 110? I que 120? 


public class Math2
{
    static void Main()
    {
        var result = 0.0;
        for (int i = 0; i < 1000; i++)
        {
            if (result > 100)
            {
                result = Math.Sqrt(result);
            }
            if (result < 0)
            {
                result += result * result;
            }
            result += 20.2;
        }
        Console.WriteLine("el resultat es $ " + result);
        // Cuan i es == 1000 el resultat es $ 111,56230589874906
        // Supera el 110 pero mai supera 120
    }
}
﻿// Roger Ribas
// 7/12/22
//// Descripcio: Ens han demanat que solucionem els errors sintàctics i semàntics via depurador d' un programa que faci el següent:
//Given two colors returns the name of the team represented by the colors.
//The correct values are:
//Team1: white, green
//Team2: white, blue,
//Team3: white, brown
//Team4: red, blue
//Team5: red, black
//Team6: green, red

using System.Drawing;

public class TeamColors
{
    static void Main()
    {

        string color1 = Console.ReadLine();
        string color2 = Console.ReadLine();
        if (color1 == "white")
        {
            if (color2 == "green") Console.WriteLine("Team1");
            if (color2 == "blue") Console.WriteLine("Team2");
            if (color2 == "brown") Console.Write("Team3");
        }
        if (color1 == "red")
        {
            if (color2 == "blue")
            {
                Console.WriteLine("Team4");
            }
            if (color2 == "black")
            {
                Console.WriteLine("Team5");

            }
        }
        if (color1 == "green" && color2 == "red") Console.WriteLine("Team6");

        else Console.WriteLine("ERROR");
        //He corregit "brown" i els string estaven mal escrits ademes he creat un altre if nomes per el color 1 green
    }
}

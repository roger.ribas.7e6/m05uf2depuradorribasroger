﻿// Roger Ribas
// 7/12/22
// Descripcio: Tenim aquest codi que dóna problemes, ho pots solucionar? 


public class DebugFor
{
    static void Main()
    {
        long numero1 = 548745184;
        long numero2 = 25145;
        long result = 0;
        for (int i = 0; i < numero2; i++)
        {
            result += numero1;
        }
        // hem de canviar el 'var' per 'long' ja que el valor donat es massa gran per a 'var'
        Console.WriteLine("la multiplicació de {0} i {1} es {2}", numero1, numero2, result);
    }

}